from django.shortcuts import render
from .models import Member, Paragraph, Event
from landing.forms import ContactForm
from django.core.mail import EmailMessage
from django.shortcuts import redirect
from django.template.loader import get_template


# Create your views here.
def index(request):
    paragraphs = Paragraph.objects.all()
    events = Event.objects.all()
    members = Member.objects.all()

    context = {
        'paragraphs': paragraphs,
        'events': events,
        'members': members,
    }

    form_class = ContactForm

    if request.method == 'POST':
        form = form_class(data=request.POST)

        if form.is_valid():
            contact_name = request.POST.get('Full_name', '')
            contact_purpose = request.POST.get('Purpose', '')
            contact_email = request.POST.get('Email_address', '')
            form_content = request.POST.get('Message', '')

            # Email the profile with the
            # contact information
            template = get_template('landing/contact_template.txt')
            context.update({
                'contact_name': contact_name,
                'contact_purpose': contact_purpose,
                'contact_email': contact_email,
                'form_content': form_content,
            })
            content = template.render(context)

            email = EmailMessage(
                contact_purpose + ' ',
                content,
                "Your website" + '',
                ['daventuro@gmail.com'],
                headers={'Reply-To': contact_email}
            )
            email.send()
            return redirect('/landing')

    return render(request, 'landing/index.html', {
        'form': form_class, 'paragraphs': paragraphs,
        'events': events, 'members': members})
