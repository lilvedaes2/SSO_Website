from django.db import models


class Paragraph(models.Model):
    name = models.CharField(max_length=100)
    content = models.TextField()

    def __str__(self):
        return self.name


class Event(models.Model):
    name = models.CharField(max_length=300)
    short_description = models.CharField(max_length=400)
    long_description = models.TextField()
    image = models.URLField()

    def __str__(self):
        return self.name


class Member(models.Model):
    name = models.CharField(max_length=100)
    rank = models.CharField(max_length=100)
    description = models.TextField()
    image = models.URLField()
    date_added = models.DateField()

    def __str__(self):
        return self.name
