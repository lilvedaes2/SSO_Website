from django import forms


class ContactForm(forms.Form):
    cList = ["I want to ask a question.", "I want to be a member.", "I want help with my Spanish homework.", "Other."]
    Full_name = forms.CharField(required=True, widget=forms.TextInput(
            attrs={
                'placeholder': 'John Doe'
            }))
    Email_address = forms.EmailField(required=True, widget=forms.EmailInput(
            attrs={

                'placeholder': 'yourMail@example.com'
            }))
    Purpose = forms.ChoiceField(choices=[(x, x) for x in cList], required=True, widget=forms.Select(
            ))
    Message = forms.CharField(
        required=True,
        widget=forms.Textarea(attrs={
                'placeholder': 'Write your message here'
            })
    )
