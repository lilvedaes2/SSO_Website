
Contact Name:
{{ contact_name }}

Subject:
{{ contact_purpose }}

Email:
{{ contact_email }}

Content:
{{ form_content|safe|striptags }}